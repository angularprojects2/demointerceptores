import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'Angular 9';
  msj: string;
  constructor(private http: HttpClient) {
    this._http.get('https://jsonplaceholder.typicode.com/todos/1').subscribe(data => {
      console.log(data);
      this.msj = 'Http Call is success from compoennt';
    }, (error) => {
      this.msj = 'Http Call is failed from component';
    });
  }
}
