import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor,HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor() {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       const token: string = 'token132';
        req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
        console.log(req.headers.get('Authorization'));
        return next.handle(req);
    }
}
